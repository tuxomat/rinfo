# rinfo - radio info
A utility that shows what is playing right now on selected radio stations

## General info
* rinfo looks up the web what is playing right now on a selected radio station.
* The supported stations are shown with `rinfo -l`
* If a station needs cookies and _HTTP::Cookies_ is installed, cookies are accepted
and deleted at program termination. Cookies are only used if the station needs them.
* Some stations need an Useragent to be transmitted. If so, an appropriate UA is hardcoded.
* If _Text::Unidecode_ and _HTML::Entities_ are installed, rinfo supports HTTP entities.
* The web site shown with `rinfo -i` isn't necessarily the home page of the station.
* If you want to support further stations, add a new section
in the hash `$cfg` and implement a function for parsing the web response. Take a look
at the code, it's not that difficult.

## Usage
    $ rinfo -h

    This is rinfo version 20190429.1

    What plays right now on the radio?

    usage: rinfo [OPTIONS]
              -d:     debug: show http response
              -l:     list known radio stations
              -i:     show info about selected station
              -h:     you're reading it!
              -s: <s> select radio station <s> (default: dyl)
              -v:     show version info and exit

## Examples

### query the default station
    $ rinfo

    Now playing on Dylan Radio:

    Title:  I Shall Be Free No. 10
    Artist: Bob Dylan

    Coming up next:
    - Saving Grace
    - Tough Mama

### query a selected station
    $ rinfo -s mo

    Now playing on Metal Only:

    Title:   Set Fire To The Sky
    Artist:  Hypnos
    Program: Keine Grusse und Wunsche moglich.
    Genre:   Mixed Metal

### show implemented radio stations
    $ rinfo -l

    885     : 885fm
    bach    : Bach Radio
    beat    : Beatles Radio
    dyl     : Dylan Radio
    mo      : Metal Only
    paradise: radio paradise

### show info about a certain radio station
    $ rinfo -i -s paradise

    name    : radio paradise
    url     : https://legacy.radioparadise.com/rp3.php?name=Music
    agent   : none needed
    cookies : not needed

## Note for users of arch linux
Download _PKGBUILD_ from this repo and execute

`makepkg -sic`

to download and install rinfo. For this to work you must have _git_ installed.
